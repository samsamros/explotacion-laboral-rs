# Una tasa aproximada de explotación

Este proyecto es un trabajo conjunto entre profesores y estudiantes. Lxs profesores nunca dejamos de aprender y el trabajo conjunto y la reciprocidad son principios fundamentales de la colectividad. Este proyecto surgió a partir de la actividad curricular realizada en clase (https://doi.org/10.17613/hqae-he70) en la que estudiantes determinaron que, como profesionistas útiles para la nación -como dicen los estatutos universitarios-, había que socializar la información y hacerla replicable.
```
Equipo de trabajo:

Adriana Martínez Rodriguez
Edwin Valencia Galván
Erick Romero Morales
Hannia Paola Martínez Olmos
Jesús Abraham Jiménez Olvera
Martha Jocelyne De Los Ríos Gómez
Octavio Rosas Landa Ramos
Samuel Rosado Zaidi
```

El paquete de datos incluye todos las hojas de cálculo de la Encuesta Mensual de la Industria Manufacturera para cada entidad federativa y usando un salario promedio como calculado en la Encuesta Nacional de Ocupación y Empleo (ENOE). Estos datos pueden ser encontrados en la carpeta **datos-emim-2018**.

En la carpeta script se encuentra el código en R para procesar los datos oficiales que se encuentran en *data*. En esta última carpeta se encuentra un archivo de metadatos con todas las fuentes y ligas de dónde se obtuvieron los datos. Además de la carpeta de *data* se encuentra la de resultados, en la que el código de R deposita y procesa la información. 

En esta carpeta de resultados, además, se encuentran varios archivos y carpetas. En la carpeta de cuadros estadísticos se encuentran todos los datos de la ENOE respecto a la situación laboral de lxs trabajadores manufactureros a nivel nacional y en el municipio de Ciudad Juárez, Chihuahua. Adicionalmente, lxs usuarios podrán encontrar los gráficos de estos cuadros estadísticos en la carpeta **gráficos** > **ENOE2022**; además de Ciudad Juárez también podrán ver los gráficos de la entidad de Tabasco.

En la carpeta principal de **resultados** se podrá encontrar una carpeta de metadatos que describen las cinco hojas de cálculo en la carpeta de **metadatos**.

La hoja de cálculo ***Comparativos2018_tablas.csv*** proviene de los datos calculados a partir de las hojas de cálculo ubicadas en **datos-emim-2018**, la ENOE de 2018, Exportaciones por Entidad Federativa y tipo de cambio según Banco de México. En el caso de ***Comparativos2018_sintablas.csv*** estos datos provinieron de la ENOE 2018 y de los datos contenidos en la serie de tiempo **tr_variable_total_entidad_mensual_2007_01_2019_02**, además de las exportaciones y el tipo de cambio. Estos datos ya fueron procesados en su totalidad por el código en este git y sin intermediación de las hojas de cálculo. 

La hoja de cálculo ***Explotacion2008-2018.csv*** fue procesada únicamente con las Exportaciones y **tr_variable_total_entidad_mensual_2007_01_2019_02**. De este modo se logró calcular tasas aproximadas de explotación sin utilizar un dato intermedio de la ENOE. Esto sirve para cotejar nuestra aproximación en las hojas de cálculo. 

En las hojas ***salarios_manuf_ENOE2018.csv*** y ***salarios_manuf_ENOE2022.csv*** se utilizaron los datos de la ENOE de 2018-IV y 2022-IV para calcular los salarios promedios a nivel estatal para cada identificación sexogenérica (únicamente binaria para INEGI): hombre y mujer. Además, se incluyó el último decil salarial para cada entidad para contrastar y para realizar el ejercicio de tasa aproximada de explotación con los salarios promedios más altos. En estos casos aún hay tasas altas.

Este sitio continúa en construcción y recibe constante atención de estudiantes para su mantenimiento y retroalimentación.

Para utilizar el script hay que cambiar la línea `setwd("/path/script")` al directorio completo de la carpeta script de este git.

**Disclaimer: esta es una tasa aproximada de explotación, para realizar el cálculo de explotación es necesario descontar los costos del valor de la producción o, visto de otra manera, contar con los datos de ganancia**

**Conflicto de interés: Este proyecto se realizó sin financiamiento alguno**

